FROM python:3.10

WORKDIR /app

RUN apt-get update
RUN apt-get install curl -y
RUN curl -sSL https://pdm-project.org/install-pdm.py | python3 -
RUN echo "export PATH=/root/.local/bin:$PATH" >> /etc/bash.bashrc

COPY ./ ./

#RUN /root/.local/bin/pdm sync
#RUN

#ENTRYPOINT [ "python" "test.py"]